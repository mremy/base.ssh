Role Name
=========

Manage OpenSSH server configuration.

Requirements
------------

An ssh server, but if you are using Ansible...

Role Variables
--------------

sshd\_akf: list of the authorized keys files (AuthorizedKeysFile parameter)

Dependencies
------------

An OpenSSH server.

Example Playbook
----------------

    - hosts: servers
      roles:
         - { role: base_ssh }

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
